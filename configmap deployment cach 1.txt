apiVersion: apps/v1
kind: Deployment
metadata:
  name: configmap-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: lab-testing
  template:
    metadata:
      labels:
        app: lab-testing
    spec:
      containers:
        - name: labcontainer
          image: ducanh17021999/kub-first-app
          command:
          - echo "Ten khach hang=$(ten_khach_hang)"
          - echo "Cong ty=$(cong_ty)"
          - echo "Vi tri=$(vi_tri)"
          env:
          - name: ten_khach_hang
            valueFrom:
              configMapKeyRef:
                name: ducanhconfigmap
                key: user_name
          - name: cong_ty
            valueFrom:
              configMapKeyRef:
                name: ducanhconfigmap
                key: user_company       
          - name: vi_tri
            valueFrom:
              configMapKeyRef:
                name: ducanhconfigmap
                key: user_position